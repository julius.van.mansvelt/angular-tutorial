export interface Fulfillable {
    inProgress: boolean;
    fulfilled: boolean;
  }
