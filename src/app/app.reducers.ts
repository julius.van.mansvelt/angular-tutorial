import {
  ActionReducerMap,
} from '@ngrx/store';
import {CarState} from './cars/car.model';
import {carReducer} from './cars/car.reducers';


export interface AppState {
  car: CarState;
}

export const appReducers: ActionReducerMap<AppState> = {
  car: carReducer
};
