import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {Car, CarState} from './car.model';
import * as fromCarActions from './cars.actions';
import {selectCars} from './car.selectors';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Observable } from 'rxjs';
import { CarService } from './car.service';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {

  constructor(private store: Store<CarState>, private fb: FormBuilder, private carService: CarService) {
  }

  cars$: Observable<Car[]>;

  myForm: FormGroup;

  ngOnInit(): void {
    this.store.dispatch(fromCarActions.loadCars());
    this.cars$ = this.store.select(selectCars);
    this.myForm = this.fb.group({
      brand: ['', [Validators.required]],
      model: ['', [Validators.required]],
      year: [1990, [Validators.required]]
    });
    this.myForm.valueChanges.subscribe(value => console.log(value));
  }

  onSubmit = () => {
    console.log(this.carService.getCars());
    this.store.dispatch(fromCarActions.addCar({
         car: {
          model: this.model.value,
          brand: this.brand.value,
          year: this.year.value
         }
        
    }));
  };

  removeCar(car: Car) { 
    console.log(car);
    this.store.dispatch(fromCarActions.removeCar({model: car.model}));
  }

  get model() {
    return this.myForm.get('model');
  }

  get brand() { 
    return this.myForm.get('brand');
  }

  get year() { 
    return this.myForm.get('year');
  }
}
