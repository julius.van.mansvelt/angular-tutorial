import {createReducer, on} from '@ngrx/store';
import {CarState} from './car.model';
import { loadCarsFulfilled, loadCars, removeCar, removeCarFulfilled, addCar, addCarFulfilled } from './cars.actions';


export const initialState: CarState = {
  cars: [],
  fulfilled: false,
  inProgress: false

};
export const carReducer = createReducer(
  initialState,

  on(addCar, (state, {car}) => ({
    cars: [...state.cars, car],
    fulfilled: true,
    inProgress: false
  })),
  on(addCarFulfilled, state => ({
    ...state,
    fulfilled: true,
    inProgress: false
  })),
  on(loadCars, state => ({
    ...state,
    fulfilled: false,
    inProgress: true
  })),
  on(loadCarsFulfilled, (state, { cars }) => ({
    cars: [...state.cars, ...cars],
    fulfilled: true,
    inProgress: false
  })),
  on(removeCar, (state, { model }) => ({ 
    cars: [...state.cars].filter(car => car.model != model),
    fulfilled: false,
    inProgress: true
  })),
  on(removeCarFulfilled, state => ({
    ...state,
    fulfilled: true,
    inProgress: false
  }))
  );
