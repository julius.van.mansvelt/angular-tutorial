import { Fulfillable } from '../shared.models';

export interface CarState extends Fulfillable {
  cars: Car[];
}

export interface Car {
  model: string;
  brand: string;
  year: number;
}
