import {createAction, props} from '@ngrx/store';
import {Car} from './car.model';

export const loadCars = createAction(
  '[Cars] Load Cars'
);

export const loadCarsFulfilled = createAction(
  '[Cars] Load Cars fulfilled',
  props<{ cars: Car[] }>()
);

export const loadCarsFailure = createAction(
  '[Cars] Load Cars Failure',
  props<{ error: any }>()
);

export const addCar = createAction('[Cars] Add Car', props <{car: Car}>())
export const addCarFulfilled = createAction('[Cars] Add Car fulfilled')

export const removeCar = createAction('[Cars] Remove Car', props <{model: string}>())
export const removeCarFulfilled = createAction('[Cars] Remove Car fulfilled')
