import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { AppState } from '../app.reducers';
import * as fromCarActions from './cars.actions';
import { CarService } from './car.service';
import {map, switchMap, withLatestFrom, mergeMap, catchError} from 'rxjs/operators';
import { EMPTY } from 'rxjs';
@Injectable()
export class CarsEffects {


  constructor(private actions$: Actions, private store: Store<AppState>, private carService: CarService) {}


  getCars = createEffect( () => this.actions$.pipe(
    ofType(fromCarActions.loadCars),
    switchMap( () => this.carService.getCars()),
    map((test) => fromCarActions.loadCarsFulfilled({cars: test}))
  ));
}
