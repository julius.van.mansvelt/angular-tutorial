import {CarState} from './car.model';
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {AppState} from '../app.reducers';

export const selectCarState = createFeatureSelector<CarState>('car');
export const selectCars = createSelector(selectCarState, carState => carState.cars);


